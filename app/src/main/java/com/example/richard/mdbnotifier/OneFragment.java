package com.example.richard.mdbnotifier;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.HashMap;


public class OneFragment extends Fragment {
    RecyclerView notificationDisplayRecyclerReview;
    static Button tempTimeSetButton;
    SwipeRefreshLayout swipeRefreshLayout;
    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflated = inflater.inflate(R.layout.fragment_one, container, false);

        notificationDisplayRecyclerReview = (RecyclerView) inflated.findViewById(R.id.notificationDisplayRecyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) inflated.findViewById(R.id.swipeRefreshLayout);

        notificationDisplayRecyclerReview.setLayoutManager(new LinearLayoutManager(SnoozeActivity.context, LinearLayoutManager.VERTICAL, false));
        notificationDisplayRecyclerReview.setAdapter(SnoozeActivity.nAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SnoozeActivity.nAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        return inflated;
    }}
