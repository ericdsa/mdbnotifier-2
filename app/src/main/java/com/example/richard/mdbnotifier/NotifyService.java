package com.example.richard.mdbnotifier;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.service.notification.StatusBarNotification;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by Richard on 4/6/2016.
 */
public class NotifyService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("listen", "Notify prompt received!");
        Bundle extras = intent.getExtras();
        int id;
        if (!extras.containsKey("locationnotifykey")) {
            if (extras != null && ((id = extras.getInt(NotificationAdapter.RELAUNCH_KEY, -1)) != -1)) {
                Log.d("listen", "Launched with id: " + id);
                SnoozeActivity.snoozeList.remove(id);
                InfoBundle reactivate = SnoozeActivity.display.get(id);
                reactivate.deadline = null;
                SnoozeActivity.display.put(id, reactivate);
                SnoozeActivity.nAdapter.nManager.notify(id, SnoozeActivity.resendMap.get(id).build());
            }
        } else {
            Log.d("locationing", "Treat as locationing que. Notify all location notifications.");
            if (intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING, false)) {
                Log.d("locationing", "Entering.");
                for (int locId : SnoozeActivity.resendMap.keySet()) {
                    InfoBundle ib = SnoozeActivity.display.get(locId);
                    if (ib.location) {
                        SnoozeActivity.display.put(locId, new InfoBundle(null));
                        SnoozeActivity.nAdapter.nManager.notify(locId, SnoozeActivity.resendMap.get(locId).build());
                    }
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
