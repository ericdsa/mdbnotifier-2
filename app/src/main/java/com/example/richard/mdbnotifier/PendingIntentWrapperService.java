package com.example.richard.mdbnotifier;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.StatusBarNotification;
import android.util.Log;

public class PendingIntentWrapperService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("listen", "Pending intent wrapper reached!");
        Bundle extras;
        if ((extras = intent.getExtras()) != null && extras.containsKey(NotificationAdapter.PENDINGINTENT_KEY)) {
            PendingIntent wrapped = (PendingIntent) extras.get(NotificationAdapter.PENDINGINTENT_KEY);
            try {
                wrapped.send();
                int key = extras.getInt(NotificationAdapter.DISMISS_KEY);
                StatusBarNotification sbn = SnoozeActivity.notifications.get(key);
                Bundle noteExtras = sbn.getNotification().extras;
                String titlepass, descpass;
                if (noteExtras.containsKey(Notification.EXTRA_TITLE) && noteExtras.get(Notification.EXTRA_TITLE) != null) {
                    titlepass = noteExtras.get(Notification.EXTRA_TITLE).toString();
                } else {
                    titlepass = " ";
                }
                if (noteExtras.containsKey(Notification.EXTRA_TEXT)  && noteExtras.get(Notification.EXTRA_TEXT) != null) {
                    descpass = noteExtras.get(Notification.EXTRA_TEXT).toString();
                } else {
                    descpass = " ";
                }
                InfoBundle ib = SnoozeActivity.display.get(key);
                String display = " ";
                if (ib != null) {
                    display = ib.deadline.toString();
                }
                String formatted = SnoozeActivity.format(titlepass, descpass, display);
                SnoozeActivity.logHistoryEntry(formatted);
                SnoozeActivity.notifications.remove(key);
            } catch (PendingIntent.CanceledException p) {
                Log.d("shit", "PendingIntent was cancelled: " + p.getMessage());
            }
        } else {
            Log.d("shit", "Oops, no pending intent within wrapper");
        }
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
