package com.example.richard.mdbnotifier;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Richard on 4/23/2016.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.CustomViewHolder> {
    static final String RELAUNCH_KEY = "relaunchkey";
    static final String PENDINGINTENT_KEY = "pendingintentkey";
    static final String DISMISS_KEY = "dismisskey";
    static final String TIME_KEY = "timekey";

    public HistoryAdapter() {
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.history_panel, parent, false));
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SnoozeActivity.context);
        int start = sp.getInt(SnoozeActivity.INDEX_START_KEY, -1);
        int end = sp.getInt(SnoozeActivity.INDEX_END_KEY, -1);
        position = end - 1 - position;
        String formatted = sp.getString(Integer.toString(position), "null");
        if (formatted == "null") {
            Log.d("shit", "null string, aborting history population on position " + position);
            return;
        }
        String[] tokens = SnoozeActivity.tokenize(formatted);
        holder.titleTextView.setText(tokens[0]);
        holder.descriptionTextView.setText(tokens[1]);
        holder.displayTextView.setText(tokens[2]);
    }

    @Override
    public int getItemCount() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SnoozeActivity.context);
        int start = sp.getInt(SnoozeActivity.INDEX_START_KEY, -1);
        int end = sp.getInt(SnoozeActivity.INDEX_END_KEY, -1);
        Log.d("logging", "start at " + start + ", end at " + end);
        return end - start;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView, descriptionTextView, displayTextView;
        public CustomViewHolder(View view) {
            super(view);
            this.titleTextView = (TextView) view.findViewById(R.id.titleTextView);
            this.descriptionTextView = (TextView) view.findViewById(R.id.descriptionTextView);
            this.displayTextView = (TextView) view.findViewById(R.id.displayTextView);
        }
    }
}
