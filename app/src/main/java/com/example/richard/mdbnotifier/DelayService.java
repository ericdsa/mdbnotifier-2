package com.example.richard.mdbnotifier;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by Richard on 4/9/2016.
 */
public class DelayService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("please", "Snooze click registered.");
        Bundle extras;
        int cancelId = -1;
        if ((extras = intent.getExtras()) != null) {
            Log.d("listen", "Extras are valid, cancelling id " + extras.getInt(NotificationAdapter.RELAUNCH_KEY));
            cancelId = extras.getInt(NotificationAdapter.RELAUNCH_KEY);
            SnoozeActivity.snoozeList.add(cancelId);
            SnoozeActivity.nAdapter.nManager.cancel(cancelId);
        } else {
            Log.d("listen", "no extras for you");
        }

        PendingIntent notifyPendingIntent = (PendingIntent) extras.get(NotificationAdapter.PENDINGINTENT_KEY);
        if (SnoozeActivity.settings.getBoolean(NotificationAdapter.LOCATION_BOOL_KEY, true)) {
            Calendar calendar = Calendar.getInstance();
            int hourOfDay = SnoozeActivity.settings.getInt(TimePickerFragment.HOUR_KEY, 0);
            int minute = SnoozeActivity.settings.getInt(TimePickerFragment.MINUTE_KEY, 0);
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), hourOfDay, minute, 0);
            Calendar currentTime = Calendar.getInstance();
            if (currentTime.after(calendar)) {
                calendar.roll(Calendar.DAY_OF_MONTH, 1);
            }
            Log.d("listen", "Notifying at set time: " + calendar.getTime());

            SnoozeActivity.alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), notifyPendingIntent);
            InfoBundle prev = SnoozeActivity.display.get(cancelId);
            if (prev != null) {
                prev.deadline = calendar.getTime();
                SnoozeActivity.display.put(cancelId, prev);
            } else {
                SnoozeActivity.display.put(cancelId, new InfoBundle(null));
            }
        } else {
            Log.d("listen", "Assuming location delay, doing nothing here.");
            SnoozeActivity.display.put(cancelId, new InfoBundle(true));
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
