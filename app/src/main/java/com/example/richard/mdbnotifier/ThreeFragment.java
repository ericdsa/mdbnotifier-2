package com.example.richard.mdbnotifier;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;


public class ThreeFragment extends Fragment {

    public ThreeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Display the fragment as the main content.

        // items in history

        // have a line and ad value to history

        // have listeners / save button

        // save should save all values
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_three, container, false);

        // load blanketSnooze checkbox, EditTexts, and Save Button
        CheckBox blanketSnooze = (CheckBox) rootView.findViewById(R.id.blanket_snooze);



        return rootView;
    }
}
