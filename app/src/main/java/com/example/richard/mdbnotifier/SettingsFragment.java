package com.example.richard.mdbnotifier;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class SettingsFragment extends Fragment {

    private SharedPreferences settings;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        settings = SnoozeActivity.settings;

        // load blanketSnooze checkbox, EditTexts, and Save Button
        final CheckBox blanketSnooze = (CheckBox) rootView.findViewById(R.id.blanket_snooze);
        boolean isChecked = settings.getBoolean("blanket_snooze", true);
        blanketSnooze.setChecked(isChecked);

        final EditText historyText = (EditText) rootView.findViewById(R.id.input_history);
        int historyNum = settings.getInt("num_history_items", 0);
        historyText.setText(Integer.toString(historyNum));

        final Button setDefaultTime = (Button) rootView.findViewById(R.id.input_time);
        setDefaultTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "Clicking time button");
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(((SnoozeActivity) SnoozeActivity.context).getSupportFragmentManager(), "timePicker");
            }
        });

        final Button setDefaultLocation = (Button) rootView.findViewById(R.id.input_location);
        setDefaultLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SnoozeActivity.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, new NoteLocationListener());

                    /* Temporarily tracking current location as "HOME". */
                    Location pointLocation = SnoozeActivity.locationManager.getLastKnownLocation(SnoozeActivity.locationManager.GPS_PROVIDER);
                    Intent notifyIntent = new Intent(SnoozeActivity.context, NotifyService.class);
                    notifyIntent.putExtra("locationnotifykey", true);
                    SnoozeActivity.proximityIntent = PendingIntent.getService(SnoozeActivity.context, 2, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    SnoozeActivity.locationManager.removeProximityAlert(SnoozeActivity.proximityIntent);
                    if (pointLocation == null) {
                        /* Default option to prevent crashing. */
                        Log.d("locationing", "No location found, shit we're fucked");
                        Toast.makeText(SnoozeActivity.context, "Can't get your location!", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("locationing", "I'm at lat " + pointLocation.getLatitude() + " and long " + pointLocation.getLongitude());
                        SnoozeActivity.locationManager.addProximityAlert(
                                pointLocation.getLatitude(), // the latitude of the central point of the alert region
                                pointLocation.getLongitude(), // the longitude of the central point of the alert region
                                SnoozeActivity.LOCATION_RADIUS, // the radius of the central point of the alert region, in meters
                                -1, // time for this proximity alert, in milliseconds, or -1 to indicate no expiration
                                SnoozeActivity.proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
                        );
                    }
                } catch (SecurityException s) {
                    Log.d("shit", "Security exception: " + s.getMessage());
                }
            }
        });

        final RadioGroup chooseTimeLocation = (RadioGroup) rootView.findViewById(R.id.group_time_location);
        boolean group_time = settings.getBoolean("time_or_location", true);
        if (group_time) {
            chooseTimeLocation.check(R.id.radio_time);
        } else {
            chooseTimeLocation.check(R.id.radio_location);
        }

        final Button saveButton = (Button) rootView.findViewById(R.id.save_preferences);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settings.edit().putBoolean("blanket_snooze", blanketSnooze.isChecked()).apply();
                String number = ((TextView) historyText).getText().toString();
                if (number.length() > 0) {
                    int start = settings.getInt(SnoozeActivity.INDEX_START_KEY, -1);
                    int end = settings.getInt(SnoozeActivity.INDEX_END_KEY, -1);
                    settings.edit().putInt("num_history_items", Integer.parseInt(number)).apply();
                    if (end - start > Integer.parseInt(number)) {
                        settings.edit().putInt(SnoozeActivity.INDEX_START_KEY, end - Integer.parseInt(number)).apply();
                    }
                }
                boolean choose_time = true;

                // Check which radio button was clicked
                switch(chooseTimeLocation.getCheckedRadioButtonId()) {
                    case R.id.radio_time:
                        choose_time = true;
                        break;
                    case R.id.radio_location:
                        choose_time = false;
                        break;
                    default:
                        break;
                }

                settings.edit().putBoolean("time_or_location", choose_time).apply();

                Snackbar snackbar = Snackbar.make(rootView, "Saved changes", Snackbar.LENGTH_LONG);
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                snackbar.show();

                Log.d("Settings", "We saved settings!");
                Log.d("Saving Settings", Boolean.toString(settings.getBoolean("blanket_snooze", true)));
                Log.d("Saving Settings", Integer.toString(settings.getInt("num_history_items", 0)));
            }
        });

        return rootView;
    }

    public class NoteLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
        }
        public void onStatusChanged(String s, int i, Bundle b) {
        }
        public void onProviderDisabled(String s) {
        }
        public void onProviderEnabled(String s) {
        }
    }
}
