package com.example.richard.mdbnotifier;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.SharedPreferencesCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import java.util.Map;

public class NotificationListenerService extends android.service.notification.NotificationListenerService {
    @Override
    public void onCreate() {
        Log.d("listen", "Successfully created notification listener!");
        NotificationAdapter.nls = this;
        super.onCreate();
    }

    @Override
    public void onListenerConnected() {
        Log.d("listen", "Successfully connected notification listener!");
        super.onListenerConnected();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        if (sbn == null) {
            Log.d("shit", "sbn is null. ???");
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!sp.getBoolean("enable", true)) {
            Log.d("id", "ID number: " + sbn.getId());
            return;
        }

        Log.d("listen", "sbn posted with id: " + sbn.getId());
        Bundle extras = sbn.getNotification().extras;
        if (extras.containsKey(Notification.EXTRA_TITLE)) {
            String checkSnooze = extras.get(Notification.EXTRA_TITLE).toString();
            if (!checkSnooze.contains(SnoozeActivity.HEADER_TEXT)) {
                if (Build.VERSION.SDK_INT >= 21) {
                    cancelNotification(sbn.getKey());
                } else {
                    Log.d("shit", "SDK version not compatible to dismiss notification.");
                }
                Log.d("listen", "Old notification dismissed.");

                SnoozeActivity.nAdapter.processNotification(sbn, null, 1);
            }
        }
        super.onNotificationPosted(sbn);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.d("listen", "Notification deleted in system.");
        Notification nf = sbn.getNotification();
        if (nf.extras != null
                && nf.extras.containsKey(Notification.EXTRA_TITLE)
                && nf.extras.get(Notification.EXTRA_TITLE).toString().contains(SnoozeActivity.HEADER_TEXT)) {
            int removedId = sbn.getId();
            if (SnoozeActivity.snoozeList.contains(removedId)) {
                Log.d("listen", "The id " + removedId + " is snoozing. Do nothing. ");
            } else {
                Log.d("listen", "Searching for " + removedId);
                for (Map.Entry<Integer, StatusBarNotification> entry : SnoozeActivity.notifications.entrySet()) {
                    int key = entry.getKey();
                    Log.d("listen", "Comparing sbn to " + key);
                    if (removedId == key) {
                        Log.d("listen", "Corresponding entries in application removed.");
                        Bundle noteExtras = sbn.getNotification().extras;
                        String titlepass, descpass;
                        if (noteExtras.containsKey(Notification.EXTRA_TITLE) && noteExtras.get(Notification.EXTRA_TITLE) != null) {
                            titlepass = noteExtras.get(Notification.EXTRA_TITLE).toString();
                        } else {
                            titlepass = " ";
                        }
                        if (noteExtras.containsKey(Notification.EXTRA_TEXT)  && noteExtras.get(Notification.EXTRA_TEXT) != null) {
                            descpass = noteExtras.get(Notification.EXTRA_TEXT).toString();
                        } else {
                            descpass = " ";
                        }
                        InfoBundle ib = SnoozeActivity.display.get(key);
                        String display = " ";
                        if (ib != null) {
                            display = ib.deadline.toString();
                        }
                        String formatted = SnoozeActivity.format(titlepass, descpass, display);
                        SnoozeActivity.logHistoryEntry(formatted);

                        SnoozeActivity.notifications.remove(key);
                        SnoozeActivity.cancelMap.remove(key);
                        SnoozeActivity.resendMap.remove(key);
                        break;
                    }
                }
            }
        } else {
            Log.d("listen", "Not an application notification. Do not handle.");
        }
        super.onNotificationRemoved(sbn);
    }
}
