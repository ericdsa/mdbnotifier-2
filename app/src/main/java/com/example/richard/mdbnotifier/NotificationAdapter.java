package com.example.richard.mdbnotifier;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by Richard on 4/2/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.CustomViewHolder> {
    Context context;
    NotificationManager nManager;
    static NotificationListenerService nls;
    int count;

    static final String RELAUNCH_KEY = "relaunchkey";
    static final String PENDINGINTENT_KEY = "pendingintentkey";
    static final String DISMISS_KEY = "dismisskey";
    static final String LOCATION_BOOL_KEY = "time_or_location";
    static final String TIME_KEY = "timekey";

    public NotificationAdapter(Context context) {
        this.context = context;
        this.count = 0;
        this.nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_panel, parent, false));
    }


    public void processNotification(StatusBarNotification sbn, CustomViewHolder holder, int flags) {
        /* Flags is a custom field, where 1 indicates that we need to send a custom notification. */
        Log.d("listen", "Processing notification with " + (holder == null) + " and flags " + flags);
        final Notification currentNotification = sbn.getNotification();
        Bundle extras = currentNotification.extras;
        String titlepass, descpass;
        final int count = sbn.getId();
        if (extras.containsKey(Notification.EXTRA_TITLE) && extras.get(Notification.EXTRA_TITLE) != null) {
            titlepass = extras.get(Notification.EXTRA_TITLE).toString();
        } else {
            titlepass = "";
        }
        if (extras.containsKey(Notification.EXTRA_TEXT)  && extras.get(Notification.EXTRA_TEXT) != null) {
            descpass = extras.get(Notification.EXTRA_TEXT).toString();
        } else {
            descpass = "";
        }
        final String title = titlepass;
        final String desc = descpass;


        String poster = sbn.getPackageName();
        try {
            /* Attempt to extract images from notification. */
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            final Context imageLoadContext = context.createPackageContext(poster, 0);
            Drawable img = null;
            Bitmap bitmap = null;
            if (extras.containsKey(Notification.EXTRA_LARGE_ICON)) {
                Log.d("icon", "Large icon found for this notification.");
                bitmap = (Bitmap) extras.get(Notification.EXTRA_LARGE_ICON);
                bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                img = new BitmapDrawable(context.getResources(), bitmap);
            } else {
                Log.d("icon", "No large icon found for this notification.");
                int iconId = extras.getInt(Notification.EXTRA_SMALL_ICON);
                img = imageLoadContext.getResources().getDrawableForDensity(iconId, metrics.densityDpi);
                bitmap = Bitmap.createBitmap(img.getIntrinsicWidth(),
                        img.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(bitmap);
            img.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            img.draw(canvas);

            final Bitmap imgBM = bitmap;
            final Drawable imgDR = img;

            /* If there is a holder with entries to populate, do so. */
            if (holder != null) {
                holder.titleTextView.setText(title);
                holder.descriptionTextView.setText(desc);
                holder.iconImageView.setImageDrawable(imgDR);
            }

            /* Create custom snooze-app notification. */
            if (flags == 1) {
                Log.d("listen", "current notification has flags" + currentNotification.flags);
                if ((currentNotification.flags & Notification.FLAG_ONGOING_EVENT) != Notification.FLAG_ONGOING_EVENT) {
                    Log.d("listen", "Tracking notification in TreeMap with index " + count);
                    SnoozeActivity.notifications.put(count, sbn);
                    InfoBundle displayMe;
                    displayMe = new InfoBundle(null);
                    SnoozeActivity.display.put(count, displayMe);
                } else {
                    Log.d("listen", "Notification is persistent. Ignoring. ");
                    return;
                }

                Intent launchIntent = new Intent(context, SnoozeActivity.class);
                launchIntent.putExtra(SnoozeActivity.DISMISS_KEY, count);
                Log.d("listen", "Storing id to close: " + count);
                Intent notifyIntent = new Intent(context, NotifyService.class);
                Log.d("listen", "ID for notifyIntent: " + count);
                notifyIntent.putExtra(RELAUNCH_KEY, count);
                notifyIntent.setAction(Integer.toString(count));
                PendingIntent notifyPendingIntent = PendingIntent.getService(context, 0, notifyIntent, 0);
                Intent delayIntent = new Intent(context, DelayService.class);
                delayIntent.putExtra(PENDINGINTENT_KEY, notifyPendingIntent);
                Log.d("listen", "ID for delayIntent: " + count);
                delayIntent.putExtra(RELAUNCH_KEY, count);
                delayIntent.setAction(Integer.toString(count));
                PendingIntent delayPendingIntent = PendingIntent.getService(context, 1, delayIntent, 0);

                SnoozeActivity.cancelMap.put(count, notifyPendingIntent);

                Intent wrapperIntent = new Intent(context, PendingIntentWrapperService.class);
                wrapperIntent.putExtra(DISMISS_KEY, count);
                wrapperIntent.putExtra(PENDINGINTENT_KEY, sbn.getNotification().contentIntent);
                PendingIntent wrapperPending = PendingIntent.getService(context, 2, wrapperIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Notification.Builder mBuilder =
                        new Notification.Builder(context)
                                .setSmallIcon(R.drawable.ic_notifications_24dp)
                                .setLargeIcon(imgBM)
                                .setContentTitle(SnoozeActivity.HEADER_TEXT + title)
                                .setContentText(desc)
                                .setContentIntent(wrapperPending)
                                .setAutoCancel(true);

                mBuilder.addAction(R.drawable.ic_notifications_24dp, "Snoozify", delayPendingIntent);
                if (currentNotification.actions != null) {
                    for (int i = 0; i < 2 && i < currentNotification.actions.length; i++) {
                        Notification.Action copy = currentNotification.actions[i];
                        if (Build.VERSION.SDK_INT >= 23) {
                            Notification.Action.Builder newAction = new Notification.Action.Builder(copy.getIcon(), copy.title, copy.actionIntent);
                            mBuilder.addAction(newAction.build());
                        } else {
                            Notification.Action.Builder newAction = new Notification.Action.Builder(R.drawable.ic_notifications_24dp, copy.title, copy.actionIntent);
                            mBuilder.addAction(newAction.build());
                        }
                    }
                }
                SnoozeActivity.resendMap.put(count, mBuilder);
                if (!SnoozeActivity.settings.getBoolean("blanket_snooze", false)) {
                    nManager.notify(count, mBuilder.build());
                } else {
                    try {
                        delayPendingIntent.send();
                    } catch (PendingIntent.CanceledException c) {
                        Log.d("shit", "Cancelled exception when prefiring PendingIntent: " + c.getMessage());
                    }
                }
                Log.d("listen", "Sent modified snooz-fication. title: " + title + ", desc: " + desc + ", id: " + Integer.toString(count - 1));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("listen", "Image not found during retrieval.");
        }
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        position = getItemCount() - position - 1;
        final StatusBarNotification sbn = (StatusBarNotification) SnoozeActivity.notifications.values().toArray()[position];
        final int key = (Integer) SnoozeActivity.notifications.navigableKeySet().toArray()[position];
        processNotification(sbn, holder, 0);

        InfoBundle displayMe = SnoozeActivity.display.get(key);
        String displayString;
        if (displayMe.deadline != null) {
            displayString = displayMe.deadline.toString();
        } else if (displayMe.location) {
            displayString = "location snooze";
        } else {
            displayString = "currently active";
        }
        holder.displayTextView.setText(displayString);
        holder.itemView.setOnTouchListener(new OnSwipeTouchListener(context) {
            @Override
            public void onSwipeLeft() {
                Log.d("listen", "Deleting alarm for id " + key + ": " + SnoozeActivity.cancelMap.get(key).toString());
                SnoozeActivity.alarmManager.cancel(SnoozeActivity.cancelMap.get(key));
                nManager.cancel(key);

                Bundle noteExtras = sbn.getNotification().extras;
                String titlepass, descpass;
                if (noteExtras.containsKey(Notification.EXTRA_TITLE) && noteExtras.get(Notification.EXTRA_TITLE) != null) {
                    titlepass = noteExtras.get(Notification.EXTRA_TITLE).toString();
                } else {
                    titlepass = " ";
                }
                if (noteExtras.containsKey(Notification.EXTRA_TEXT)  && noteExtras.get(Notification.EXTRA_TEXT) != null) {
                    descpass = noteExtras.get(Notification.EXTRA_TEXT).toString();
                } else {
                    descpass = " ";
                }
                InfoBundle ib = SnoozeActivity.display.get(key);
                String display = Calendar.getInstance().getTime().toString();
                Log.d("logging", "logging: titlepass: " + titlepass + ", desc: " + descpass + ", display: " + display);
                String formatted = SnoozeActivity.format(titlepass, descpass, display);
                Log.d("logging", "logged formatted: " + formatted);
                SnoozeActivity.logHistoryEntry(formatted);

                SnoozeActivity.display.remove(key);
                SnoozeActivity.notifications.remove(key);
                SnoozeActivity.cancelMap.remove(key);
                SnoozeActivity.resendMap.remove(key);
                notifyDataSetChanged();
                Toast.makeText(context, "Dismissing notification from app.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSwipeRight() {
                Log.d("listen", "Reinstating notification with id " + sbn.getId());
                SnoozeActivity.alarmManager.cancel(SnoozeActivity.cancelMap.get(key));
                InfoBundle newIB = new InfoBundle(null);
                SnoozeActivity.display.put(sbn.getId(), newIB);
                nManager.notify(sbn.getId(), SnoozeActivity.resendMap.get(sbn.getId()).build());
                notifyDataSetChanged();
                Toast.makeText(context, "Re-notifying!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return SnoozeActivity.notifications.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView, descriptionTextView, displayTextView;
        ImageView iconImageView;
        public CustomViewHolder(View view) {
            super(view);
            this.titleTextView = (TextView) view.findViewById(R.id.titleTextView);
            this.descriptionTextView = (TextView) view.findViewById(R.id.descriptionTextView);
            this.iconImageView = (ImageView) view.findViewById(R.id.iconImageView);
            this.displayTextView = (TextView) view.findViewById(R.id.displayTextView);
        }
    }
}
