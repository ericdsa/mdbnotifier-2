package com.example.richard.mdbnotifier;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TwoFragment extends Fragment {
    RecyclerView historyDisplayRecyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflated = inflater.inflate(R.layout.fragment_two, container, false);

        historyDisplayRecyclerView = (RecyclerView) inflated.findViewById(R.id.historyDisplayRecyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) inflated.findViewById(R.id.swipeRefreshLayout);

        historyDisplayRecyclerView.setLayoutManager(new LinearLayoutManager(SnoozeActivity.context, LinearLayoutManager.VERTICAL, false));
        historyDisplayRecyclerView.setAdapter(SnoozeActivity.hAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SnoozeActivity.hAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        return inflated;
    }
}
