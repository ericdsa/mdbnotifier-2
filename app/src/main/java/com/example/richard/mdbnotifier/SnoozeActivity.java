package com.example.richard.mdbnotifier;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.service.notification.StatusBarNotification;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

public class SnoozeActivity extends AppCompatActivity {
    final static float LOCATION_RADIUS = 100; // Radius in which we want to send notifications
    final static String HEADER_TEXT = "Snooze: ";
    final static String DISMISS_KEY = "dismissidcount";
    final static String INDEX_START_KEY = "inclusiveindexstartkey";
    final static String INDEX_END_KEY = "exclusiveindexendkey";
    final static String HISTORY_LEN_KEY = "num_history_items";
    final static String DIVIDER = "~!@#";

    static TreeSet<Integer> snoozeList; //Tracks which notifications are snoozing, to handle dismissal.
    static HashMap<Integer, InfoBundle> display; //Tracks information to be displayed for each notification.
    static TreeMap<Integer, StatusBarNotification> notifications; //Tracks currently snoozing notifications.
    static HashMap<Integer, Notification.Builder> resendMap; //Tracks NotificationCompat.Builders to reconstruct notifications to resend when time elapses.
    static HashMap<Integer, PendingIntent> cancelMap; //Tracks PendingIntents mapped to IDs to cancel with AlarmManager when desired.
    static NotificationAdapter nAdapter;
    static HistoryAdapter hAdapter;
    static LocationManager locationManager;
    static AlarmManager alarmManager;
    public static SharedPreferences settings;
    static Context context;
    static PendingIntent proximityIntent;

    Location pointLocation;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        context = SnoozeActivity.this;

        /* Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(intent); */
        /* Instantiate static variables. */
        if (notifications == null) {
            notifications = new TreeMap<>();
        }
        if (cancelMap == null) {
            cancelMap = new HashMap<>();
        }
        if (snoozeList == null) {
            snoozeList = new TreeSet<>();
        }
        if (alarmManager == null) {
            alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        }
        if (display == null) {
            display = new HashMap<>();
        }
        if (nAdapter == null) {
            nAdapter = new NotificationAdapter(SnoozeActivity.this);
        }
        if (hAdapter == null) {
            hAdapter = new HistoryAdapter();
        }
        if (resendMap == null) {
            resendMap = new HashMap<>();
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        settings = sp;
        SharedPreferences.Editor spe = sp.edit();
        spe.putBoolean("enable", true);
        if (sp.getInt(INDEX_START_KEY, -1) == -1) {
            spe.putInt(INDEX_START_KEY, 0);
        }
        if (sp.getInt(INDEX_END_KEY, -1) == -1) {
            spe.putInt(INDEX_END_KEY, 0);
        }
        if (sp.getInt(HISTORY_LEN_KEY, -1) == -1) {
            spe.putInt(HISTORY_LEN_KEY, 100);
        }
        spe.apply();

        if (Settings.Secure.getString(this.getContentResolver(),"enabled_notification_listeners").contains(getApplicationContext().getPackageName()))
        {
            //service is enabled do something
        } else {
            //service is not enabled try to enabled by calling...
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }

        /* Set up the LocationManager and get location updates. TO BE MOVED WHEN WE TOGGLE LOCATIONS */
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null) {
            Log.d("shit", "No location manager found.");
        }

        Intent listenerIntent = new Intent(SnoozeActivity.context, NotificationListenerService.class);
        SnoozeActivity.context.startService(listenerIntent);
        Log.d("listen", "Started intent for notification listener!");
    }

    @Override
    protected void onResume() {

        Log.d("listen", "Refreshing recycler view with " + notifications.size() + " items.");
        nAdapter.notifyDataSetChanged();
        super.onResume();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "Snoozing");
        adapter.addFragment(new TwoFragment(), "History");
        adapter.addFragment(new SettingsFragment(), "Settings");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public static void logHistoryEntry (String formatted) {
        Log.d("logging", "Logging history entry: " + formatted);
        int start, end, len;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor spe = sp.edit();
        if ((start = sp.getInt(INDEX_START_KEY, -1)) == -1) {
            Log.d("shit", "No start index, abort mission.");
        }
        if ((end = sp.getInt(INDEX_END_KEY, -1)) == -1) {
            Log.d("shit", "No end index, abort mission.");
        }
        if ((len = sp.getInt(HISTORY_LEN_KEY, -1)) == -1) {
            Log.d("shit", "No history length, abort mission.");
        }
        spe.putString(Integer.toString(end), formatted);
        end = end + 1;
        spe.putInt(INDEX_END_KEY, end);
        if (end - start > len) {
            spe.putInt(INDEX_START_KEY, end - len);
        }
        spe.apply();
    }

    public static String format(String title, String desc, String display) {
        String formatted = new String();
        formatted = formatted.concat(title).concat(DIVIDER).concat(desc).concat(DIVIDER).concat(display);
        return formatted;
    }

    public static String[] tokenize(String formatted) {
        String[] tokens = new String[3];
        String[] splitter = formatted.split(DIVIDER);
        if (splitter.length != 3) {
            Log.d("shit", "Invalidly formatted string. Had " + formatted);
        } else {
            tokens = splitter;
        }
        return tokens;
    }
}
