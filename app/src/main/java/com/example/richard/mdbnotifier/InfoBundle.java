package com.example.richard.mdbnotifier;

import java.util.Date;

/**
 * Created by Richard on 4/20/2016.
 */
public class InfoBundle {
    Date deadline; // When the notification is snoozed until
    boolean location;
    public InfoBundle(Date d) {
        location = false;
        deadline = d;
    }
    public InfoBundle(boolean location) {
        this.location = location;
        deadline = null;
    }
}
